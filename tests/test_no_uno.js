
function CCM_Child(){
    
    //declarations
    var priv = {};
    //private variables
    priv.priv_var_1 = '';
    //private methods
    var prot = {};
    //protected variables
    prot.prot_var_1 = '';
    //protected methods
    //public variables
    this.pub_var_1 = '';
    //public methods
    this.myMagic = function ( args ){
        if (typeCheck (new Array('goodtype', 'badtype','a','a'), args)) {
            alert ( 'Typecheck passed' );
        }
    }
    this.set_pub_var_1 = function ( pub_var) {
        this.pub_var_1 = pub_var;
    }
    this.get_pub_var_1 = function () {       
        return this.pub_var_1;
    };
    //extension code
    //set the parent
    priv.parent = new CCM_Class();
    //create this object's extend method for possible children
    this.extend = priv.parent.extender(prot);
    //extend this object from the parent
    priv.parent.extend ( this, prot );
}


test.myMagic( new Array ( new CCM_Child, new Date(), 1, 'a'));

